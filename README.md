### Puppet là gì ?
Là tool cấu hình tự động, cùng loại với ansible, chef auto, vì Puppet có thể truyền file, folder và làm nhiều task system khác nên có thể dùng vừa là configuration tools và deloploy tool trên Linux.

Puppet có thể thực thi cấu hình cho Windows Server nhưng tác giả chưa thử.

Có loại Puppet Enterprise nhưng tác giả chưa so sánh và chưa tìm hiểu về giá cả.

### Cơ chế vận hành:

Có 2 kiểu chạy, local và master-agent.
Có thể dùng puppet chỉ để chạy cho máy tính local thôi, chủ yếu để backup lại cấu hình của server đó.

Puppet master và agent, toàn bộ cấu hình group server đều nằm tại master, sau đó agent kéo về theo chu kì được định sẵn, vì dụ như 10p / lần chẳng hạn, lúc này agent chạy như một service trên linux, tuy nhiên có thể kéo cấu hình về ngay lập tức bằng cách gõ lệnh mà không cần chờ.

### Infrastructure As A Code:
Cũng y chang các hãng khác, tool này giúp cho devops cài đặt nhiều máy chủ chỉ bằng viết code. Đến đây chúng ta bắt đầu quan tâm Puppet làm được gì ? có bao nhiêu hàm ra lệnh ? viết code thì cấu trúc classes, modules, reuse , khai biến môi trường, viết điều kiện thế nào ? Từ từ đi, cài đặt đã.

**Xem cách cài đặt cơ bản trong thư mục Document, có phần tạo môi trường test thử bằng docker compose**



