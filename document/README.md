Người viết: Phú
Skype: phulerock


## Cài đặt cơ bản:
Phiên bản: 4.10
OS: ubuntu 16.04
Các phiên bản khác và OS khác tự bơi tiếp nhé.
Bài này hướng dẫn cài đặt kiểu master agent, nếu muốn cài đặt chạy 1 server standalone, thì cái gói puppet thôi.

Mô hình đơn giản:
1 Puppetmaster ==> 1 Puppetagent

Chú ý: Master ăn rất nhiều RAM, nên nếu chạy máy ảo thì cấp 3Gb ram trở lên.

**1. Tại Puppetmaster:** quyền root

```bash
curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
sudo dpkg -i puppetlabs-release-pc1-xenial.deb
sudo apt-get update
sudo apt-get install -y puppetserver
```

Cấp thêm RAM cho puppet master để cho chắc ăn, không làm cũng ko sao, đó là cấu hình lại dòng
```
JAVA_ARGS="-Xms3g -Xmx3g -XX:MaxPermSize=256m"
```
bằng lệnh:

```
sed s,2g,3g,g /etc/default/puppetserver
```

Puppet master chạy https port 8140, phải mở filewall port 8140:

```
sudo ufw allow 8140
```

Chỉnh trong /etc/hosts, lí do là puppet hoạt động trên hostname, ko phải ip, nên nếu không dùng DNS local thì phải add host. Ví dụ dười đây thì master có hostname là "puppet", còn agent là "puppetagent"
```
172.16.1.91     puppetagent
172.16.1.90     puppet
```
cấu hình cho server, vào file /etc/puppetlabs/puppet/puppet.conf, thêm phần [main] là cấu hình tổng quát, phần master là cấu hình riêng cho tính năng master:

```
[main]
certname = puppetmaster
server = puppet
environment = production
runinterval = 1h
strict_variables = true
[master]
vardir = /opt/puppetlabs/server/data/puppetserver
logdir = /var/log/puppetlabs/puppetserver
rundir = /var/run/puppetlabs/puppetserver
pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid
codedir = /etc/puppetlabs/code
```
**Ghi nhớ:** "server =" là hostname của máy tính hiện tại, certname là tên mình tự đặt cho certificate riêng cho máy tính ấy, sẽ xuất hiện trong lệnh puppet cert list --all, đây là cert key để master và agent bắt tay với nhau.

Start Puppet master

systemctl start puppetserver
Nếu không start được, dùng lệnh
service puppetserver start


**2. Tại agent:**
```bash
curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
sudo dpkg -i puppetlabs-release-pc1-xenial.deb
sudo apt-get update
sudo apt-get install puppet-agent
```

Chỉnh hostname như lúc làm trên master:
```bash
172.16.1.91     puppetagent
172.16.1.90     puppet
```

cấu hình cho agent, cũng vào /etc/puppetlabs/puppet/puppet.conf chỉnh phần main thôi là đủ, những thứ còn lại ko cần thay đổi, chỉ phần server trỏ về master bằng hostname "puppet":
```bash
[agent]
certname = puppetagent
server = puppet
environment = production
runinterval = 1h
strict_variables = true
```
`systemctl start puppetagent`
hoặc
`service puppetagent start`

**3. Tại master:**
add agent vào để quản lí, dùng tên certname của agent là puppetagent
```
puppet cert sign puppetagent
```
hoặc sign tất cả
```
puppet cert sign --all
```
kiểm tra bằng
```
puppet cert list --all
```

**4. Tại agent:**
kiểm tra kết nối:
```
puppet agent --server=puppet --test
```
hoặc
```
puppet agent --server=puppet --test --debug
```

**5. Tại master:**
tạo thử 1 manifest để kiểm tra thử.
tạo file `/etc/puppetlabs/code/environments/production/manifests/test.pp`

```
node "puppetagent" {
file {'/tmp/it_works.txt':                        # resource type file and filename
  ensure  => present,                             # make sure it exists
  mode    => '0644',                              # file permissions
  content => "It works on ${ipaddress_eth0}!\n",  # Print the eth0 IP fact
     }
}
```

**6. Tại agent:**
Lệnh `puppet agent` thôi cũng đủ để chạy như 1 service, thêm option chi tiết hơn để chạy 30s 1 lần, trên thực tế phải để interval 10-30 phút khi số lượng agent nhiều sẽ gây áp lực cho puppet master rất lớn.
```
puppet agent --server=puppet --runinterval=30
```

Chú ý nên để agent start lúc khởi động (tự tìm hiểu)

## cần viết tiếp:
cấu trúc thư mục, module, class là gì, resource là gì.
làm 1 mainifest đơn giản bao gồm: 
- puppet + git
- dùng biến ip của riêng if, hostname
- file (permission)
- folder
- user group
- add key cho user
- add repo
- cài lamp (vhost, sql userpass)
- viết 1 câu điều kiện chọn OS
note: 
puppet config print modulepath # print all module paths