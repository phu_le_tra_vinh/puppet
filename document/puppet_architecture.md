# Cấu trúc code của puppet
Trên server master, trong thư mục `/etc/puppetlabs/` chúng ta có thư mục `code`, bạn có thể tìm thấy trong git repo này tại docker/docker/code, cấu trúc thư mục như sau:

```
code
└── environments
    └── production
        ├── environment.conf
        ├── manifests
        │   └── lamp.pp
        └── modules
            └── lamp
                ├── files
                │   └── copied_file
                └── manifests
                    └── init.pp
```

Trong trường hợp này, agent sẽ chạy các config trong **lamp.pp**, các agent sẽ tự update cấu hình theo mô tả đó. Thay đổi config các file này, agent sẽ thay đổi theo, không cần resrat master hay agent. Nói chung, agent sẽ chạy tất cả các file .pp trong thư mục **production/manifests/*.pp**

**node** là tên các agent đang kết nối với master, 1 node gọi nhiều modules và classes lồng ghép nhau để chạy thành 1 kịch bảng cấu hình hoàn chỉnh. Xem **lamp.pp** mô tả node tên là **puppetagent**.

**lamp.pp** có 3 loại ví dụ resource về file: tạo thư mục, tạo file có nội dung, transfer file từ master sang agent (google **puppet resource type**). Phần cuối cùng gọi 1 module tên **lamp** bằng cách inclule. Module **lamp** hay module nào khác được tạo ra đều nằm trong thư mục module cùng cấp với thư mục **production/manifests/**, trường hợp này **production/manifests/*.pp** sẽ gọi các module trong **production/modules/**. 

Xem nội dung **production/modules/lamp** để hiểu cách tạo 1 module.

Trong file **lamp.pp** có mô tả truyền 1 file **copied_file** qua puppagent và đổi tên thành **/tmp/creating_dir/hello**, **copied_file** dùng trong trường hợp đẩy file từ master đến agent, sẽ được biễu diễn dạng **puppet:///tên_master/modules/lamp/copied_file** qua https port 8140. Best pratice về truyền file là tạo hẳn 1 module **fileserver** và đặt tại một dir bất kì, sau đó add đường dẫn dir vào environment.conf.

environment.conf: chứa đường dẫn của các modules.