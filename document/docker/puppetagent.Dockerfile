FROM ubuntu:16.04
MAINTAINER phultv@hvn.vn <cloud-ops@centos.org>
RUN apt-get update
RUN apt-get install -y curl
RUN cd /tmp/
RUN curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
RUN dpkg -i puppetlabs-release-pc1-xenial.deb
RUN apt-get update
RUN apt-get install -y supervisor

RUN apt-get install -y  puppet-agent
RUN apt-get install -y vim net-tools
RUN apt-get install -y iputils-ping
RUN apt-get install -y nano
ENV PATH="/opt/puppetlabs/bin/:${PATH}"
COPY puppet.agent.conf /etc/puppetlabs/puppet/puppet.conf
COPY supervisordagent.conf /etc/supervisor/supervisord.conf
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
