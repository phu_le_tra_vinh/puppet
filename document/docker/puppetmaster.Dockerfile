FROM ubuntu:16.04
MAINTAINER phultv@hvn.vn <cloud-ops@centos.org>
RUN apt-get update
RUN apt-get install -y curl
RUN cd /tmp/
RUN curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
RUN dpkg -i puppetlabs-release-pc1-xenial.deb
RUN apt-get update
RUN apt-get install -y supervisor curl
RUN apt-get install -y puppetserver
RUN apt-get install -y vim net-tools
RUN apt-get install -y iputils-ping
RUN apt-get install -y nano
ENV PATH="/opt/puppetlabs/bin/:${PATH}"
RUN sed s,2g,3g,g /etc/default/puppetserver
RUN mv /etc/puppetlabs/puppet/puppet.conf /etc/puppetlabs/puppet/puppet.conf.bk
COPY puppet.master.conf /etc/puppetlabs/puppet/puppet.conf
COPY supervisordserver.conf /etc/supervisor/supervisord.conf
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
