# Môi trường test thử puppet
Môi trường này tạo ra 1 master và 1 agent.
OS: ubuntu 16.04
Tools: có cài thêm vài tools để các bạn gõ lệnh ping hay ifconfig, netcat debug cho tiện.

Master's Dockerfile: puppetmaster
Master hostname: puppet
Master IP static: 172.16.1.90

Agent hostname: puppetagent
Agent IP static: 172.16.1.91
Agent Dockerfile: puppetagent

Về chi tiết các bước thực thi ban đầu của từng máy tính, vào Dockerfile xem kĩ nhé.
# Start môi trường lên
```
docker-compose up -d
```
Bây giờ chúng ta có 1 master và 1 agent đã chạy. Việc cần làm là vào master accept request từ agent. Để jump vào shell master có thể dùng:
```
docker exec -it docker_puppetmaster_1 bash
```
kiểm tra xem có cert nào đang chờ accept hay không:
```
puppet cert list
```
Sign request của máy tính tên là puppetagent
```
puppet cert sign puppetagent
```
Tạo 1 manifest để test `/etc/puppetlabs/code/environments/production/manifests/test.pp`
```
node "puppetagent" {
file {'/tmp/it_works.txt':                        # resource type file and filename
  ensure  => present,                             # make sure it exists
  mode    => '0644',                              # file permissions
  content => "It works on ${ipaddress_eth0}!\n",  # Print the eth0 IP fact
     }
}
```

Chỉ cần tạo ra file này, save lại thì tự động agent sẽ thực thi, không cần chạy lệnh nào.


