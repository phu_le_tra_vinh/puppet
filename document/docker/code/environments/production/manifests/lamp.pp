node puppetagent {
    file { '/tmp/creating_dir':
        ensure => directory,
        mode => '0755',
        owner => 'root',
        group => 'root',
    }
    file { '/tmp/creating_dir/hello':
        mode => "644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/lamp/copied_file',
    }
	file {'/tmp/it_works.txt':                        # creating file
        ensure  => present,                             
        mode    => '0644',                              
        content => "It works on ${ipaddress_eth0}!\n",  # Print variable 
    }
    include lamp
}
